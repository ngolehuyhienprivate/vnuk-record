<%@ page import="java.util.*, vn.edu.vnuk.record.dao.ContactDao, vn.edu.vnuk.record.jdbc.model.Contact"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Read Contact Scriptlet</title>
	</head>
	<body>
		<table>
			<tr>
				<td>Name</td>
				<td>Email</td>
				<td>Address</td>
				<td>Date of Birth</td>
			</tr>
		<% 
			ContactDao dao = new ContactDao();
			List<Contact> contacts = dao.read();
			for(Contact contact : contacts){
		%>
			<tr>
				<td><%= contact.getName() %></td>
				<td><%= contact.getEmail() %></td>
				<td><%= contact.getAddress() %></td>
				<td><%= contact.getDateOfBirth().getTime() %></td>
			</tr>
		<%
			}
		%>
		</table>
	</body>
</html>