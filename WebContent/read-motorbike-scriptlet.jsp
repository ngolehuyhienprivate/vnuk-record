<%@ page import="java.util.*, vn.edu.vnuk.record.dao.MotorbikeDao, vn.edu.vnuk.record.jdbc.model.Motorbike"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Read Motorbike Scriptlet</title>
	</head>
	<body>
		<table>
			<tr>
				<td>Brand</td>
				<td>Model</td>
				<td>Engine Size</td>
				<td>Gearbox</td>
			</tr>
		<% 
			MotorbikeDao dao = new MotorbikeDao();
			List<Motorbike> motorbikes = dao.read();
			for(Motorbike motorbike : motorbikes){
		%>
			<tr>
				<td><%= motorbike.getBrand() %></td>
				<td><%= motorbike.getModel() %></td>
				<td><%= motorbike.getEngineSize() %></td>
				<td><%= motorbike.getGearbox() %></td>
			</tr>
		<%
			}
		%>
		</table>
	</body>
</html>