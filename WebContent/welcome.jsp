<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<% String message = "Welcome to the 21st century!"; %>
		<h1>
			<% out.print(message); %>
		</h1>
		<br/>
		<% String developed = "Developed by Jack Sage"; %>
		<h2>
			<%= developed %>
		</h2>
		<% System.out.println("Goodbye! See you in the 22nd century!"); %>
	</body>
</html>