<%@ page import="java.util.*, vn.edu.vnuk.record.dao.ContactDao, vn.edu.vnuk.record.jdbc.model.Contact"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Read Contacts Simplified</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
		<jsp:useBean id="dao" class="vn.edu.vnuk.record.dao.ContactDao"></jsp:useBean>
		<c:import url="header.jsp"/>
		<h2> Recording contacts</h2>
		
		<table>
			<tr>
				<td>Name</td>
				<td>Email</td>
				<td>Address</td>
				<td>Date of Birth</td>
			</tr>
			
			<c:forEach var="contact" items="${dao.read()}">
				<tr>
					<td>${contact.name}</td>
					<td>
					<c:choose>
					<c:when test="${not empty contact.email}">
						<a href="mailto:${contact.email}">${contact.email}</a>
					</c:when>
					
					<c:otherwise>
						<i>No email address</i>
					</c:otherwise>
					</c:choose>
					</td>
					<td>${contact.address}</td>
					<td><fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/></td>
						
				</tr>
			</c:forEach>
			
		</table>
		
		<c:import url="footer.jsp"/>
	</body>
</html>