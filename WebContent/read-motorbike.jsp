<%@ page import="java.util.*, vn.edu.vnuk.record.dao.MotorbikeDao, vn.edu.vnuk.record.jdbc.model.Motorbike"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Read Motorbikes Simplified</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
		<jsp:useBean id="dao" class="vn.edu.vnuk.record.dao.MotorbikeDao"></jsp:useBean>
		<c:import url="header.jsp"/>
		<h2> Recording motorbikes</h2>
		<hr/>
		<table>
			<tr>
				<td>Brand</td>
				<td>Model</td>
				<td>Engine Size</td>
				<td>Gearbox</td>
			</tr>
			
			<c:forEach var="motorbike" items="${dao.read()}">
				<tr>
					<td>${motorbike.brand}</td>
					<td>${motorbike.model}</td>
					<td>${motorbike.engineSize}</td>
					<td>${motorbike.gearbox}</td>
						
				</tr>
			</c:forEach>
				
			</table>
			
			<c:import url="footer.jsp"/>
	</body>
</html>