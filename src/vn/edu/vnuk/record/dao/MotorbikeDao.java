package vn.edu.vnuk.record.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vn.edu.vnuk.record.jdbc.ConnectionFactory;
import vn.edu.vnuk.record.jdbc.model.Motorbike;


public class MotorbikeDao {
	//Properties
		private Connection connection;
		
		//Constructor
		public MotorbikeDao() throws ClassNotFoundException {
			this.connection = new ConnectionFactory().getConnection();
		}
		
		//Methods
		//CREATE
		public void create (Motorbike motorbike) throws SQLException {
			String sqlQuery = "INSERT INTO motorbikes (brand, model, engine_size, gearbox)"
					+ " values (?,?,?,?)";
			PreparedStatement statement;
			try {
				statement = connection.prepareStatement(sqlQuery);
				statement.setString(1, motorbike.getBrand());
				statement.setString(2, motorbike.getModel());
				statement.setInt(3, motorbike.getEngineSize());
				statement.setString(4, motorbike.getGearbox());
				statement.execute();
				statement.close();
				
				System.out.println("Data inserted into contacts!");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				System.out.println("Done!");
			}
		}
		
		//READ (list of contacts)
		@SuppressWarnings("finally")
		public List<Motorbike> read () throws SQLException {
			String sqlQuery = "SELECT * FROM motorbikes;";
			PreparedStatement statement;
			
			List<Motorbike> motorbikes = new ArrayList<Motorbike>();
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				ResultSet results = statement.executeQuery();
				
				while(results.next()) {
					Motorbike motorbike = new Motorbike();
					motorbike.setBrand(results.getString("brand"));
					motorbike.setModel(results.getString("model"));
					motorbike.setEngineSize(results.getInt("engine_size"));
					motorbike.setGearbox(results.getString("gearbox"));
					motorbikes.add(motorbike);
				}
				
				results.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				return motorbikes;
			}
			
		}
		
		//READ (single contact)
		@SuppressWarnings("finally")
		public Motorbike read (int id) throws SQLException {
			String sqlQuery = "SELECT * FROM motorbikes where id = ?;";
			PreparedStatement statement;
			
			Motorbike motorbike = new Motorbike();
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				statement.setInt(1, id);
	            statement.execute();
				ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					motorbike.setBrand(results.getString("brand"));
					motorbike.setModel(results.getString("model"));
					motorbike.setEngineSize(results.getInt("engine_size"));
					motorbike.setGearbox(results.getString("gearbox"));
				}
				
				results.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				return motorbike;
			}
			
		}
		
		//UPDATE
		public boolean update(Motorbike motorbike) throws SQLException {
			String sqlQuery = "UPDATE motorbikes SET brand=?, model=?, engine_size=?, gearbox=? WHERE id = ?;";
			PreparedStatement statement;

	        try {
	        	statement = connection.prepareStatement(sqlQuery);
	        	statement.setString(1, motorbike.getBrand());
				statement.setString(2, motorbike.getModel());
				statement.setInt(3, motorbike.getEngineSize());
				statement.setString(4, motorbike.getGearbox());
				statement.setLong(5, motorbike.getId());
	            
	            ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					System.out.println("Data updated into contacts!");
	            } else {
	            	System.out.println("There is no match data!");
	            }
	            return true;
	        } catch (SQLException e) {
				e.printStackTrace();
	        } finally {
	        	connection.close();
				System.out.println("Done!");
	        }
	        return false;
	    }
		
		//DELETE
		public boolean delete(int id) throws SQLException {
			String sqlQuery = "DELETE FROM motorbikes WHERE id = ?;";
			PreparedStatement statement;

	        try {
	        	statement = connection.prepareStatement(sqlQuery);
	        	statement.setInt(1, id);
	        	
	        	ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					System.out.println("1 row deleted from contacts!");
	            } else {
	            	System.out.println("There is no match data!");
	            }
				
	            return true;
	        } catch (SQLException e) {
				e.printStackTrace();
	        } finally {
	        	connection.close();
				System.out.println("Done!");
	        }
	        return false;
	    }
}
