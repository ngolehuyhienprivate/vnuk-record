package vn.edu.vnuk.record.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.edu.vnuk.record.jdbc.ConnectionFactory;
import vn.edu.vnuk.record.jdbc.model.Contact;

public class ContactDao {
		//Properties
		private Connection connection;
		
		//Constructor
		public ContactDao() throws ClassNotFoundException {
			this.connection = new ConnectionFactory().getConnection();
		}
		
		//Methods
		//CREATE
		public void create (Contact contact) throws SQLException {
			String sqlQuery = "INSERT INTO contacts (name, email, address, date_of_birth)"
					+ " values (?,?,?,?)";
			PreparedStatement statement;
			try {
				statement = connection.prepareStatement(sqlQuery);
				statement.setString(1, contact.getName());
				statement.setString(2, contact.getEmail());
				statement.setString(3, contact.getAddress());
				statement.setDate(
					4, 
					new java.sql.Date(
						contact.getDateOfBirth().getTimeInMillis()
					)
				);
				statement.execute();
				statement.close();
				
				System.out.println("Data inserted into contacts!");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				System.out.println("Done!");
			}
		}
		
		//READ (list of contacts)
		@SuppressWarnings("finally")
		public List<Contact> read () throws SQLException {
			String sqlQuery = "SELECT * FROM contacts;";
			PreparedStatement statement;
			
			List<Contact> contacts = new ArrayList<Contact>();
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				ResultSet results = statement.executeQuery();
				
				while(results.next()) {
					Contact contact = new Contact();
					contact.setId(results.getLong("id"));
					contact.setName(results.getString("name"));
					contact.setEmail(results.getString("email"));
					contact.setAddress(results.getString("address"));
					Calendar date = Calendar.getInstance();
					date.setTime(results.getDate("date_of_birth"));
					contact.setDateOfBirth(date);
					contacts.add(contact);
				}
				
				results.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				return contacts;
			}
			
		}
		
		//READ (single contact)
		@SuppressWarnings("finally")
		public Contact read (int id) throws SQLException {
			String sqlQuery = "SELECT * FROM contacts where id = ?;";
			PreparedStatement statement;
			
			Contact contact = new Contact();
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				statement.setInt(1, id);
	            statement.execute();
				ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					contact.setId(results.getLong("id"));
					contact.setName(results.getString("name"));
					contact.setEmail(results.getString("email"));
					contact.setAddress(results.getString("address"));
					Calendar date = Calendar.getInstance();
					date.setTime(results.getDate("date_of_birth"));
					contact.setDateOfBirth(date);
				}
				
				results.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				connection.close();
				return contact;
			}
			
		}
		
		//UPDATE
		public boolean update(Contact contact) throws SQLException {
			String sqlQuery = "UPDATE contacts SET name=?, email=?, address=?, date_of_birth=? WHERE id = ?;";
			PreparedStatement statement;

	        try {
	        	statement = connection.prepareStatement(sqlQuery);
	        	statement.setString(1, contact.getName());
				statement.setString(2, contact.getEmail());
				statement.setString(3, contact.getAddress());
				statement.setDate(
					4, 
					new java.sql.Date(
						contact.getDateOfBirth().getTimeInMillis()
					)
				);
	            statement.setLong(5, contact.getId());
	            
	            ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					System.out.println("Data updated into contacts!");
	            } else {
	            	System.out.println("There is no match data!");
	            }
	            return true;
	        } catch (SQLException e) {
				e.printStackTrace();
	        } finally {
	        	connection.close();
				System.out.println("Done!");
	        }
	        return false;
	    }
		
		//DELETE
		public boolean delete(int id) throws SQLException {
			String sqlQuery = "DELETE FROM contacts WHERE id = ?;";
			PreparedStatement statement;

	        try {
	        	statement = connection.prepareStatement(sqlQuery);
	        	statement.setInt(1, id);
	        	
	        	ResultSet results = statement.executeQuery();
				
				if(results.next() && results != null) {
					System.out.println("1 row deleted from contacts!");
	            } else {
	            	System.out.println("There is no match data!");
	            }
				
	            return true;
	        } catch (SQLException e) {
				e.printStackTrace();
	        } finally {
	        	connection.close();
				System.out.println("Done!");
	        }
	        return false;
	    }
}
