package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.jdbc.ConnectionFactory;


public class TestConnection extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			Connection connection = new ConnectionFactory().getConnection();	
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Test Connection</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Database connected successfully!</h1>");
			out.println("</body>");
			out.println("</html>");
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Test Connection</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Database connected failed!</h1>");
			out.println("</body>");
			out.println("</html>");
			e.printStackTrace();
		}
		
	}
}