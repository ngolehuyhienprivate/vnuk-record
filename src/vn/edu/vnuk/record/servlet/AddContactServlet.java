package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.jdbc.model.Contact;

@WebServlet("/addContact")
public class AddContactServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String dateAsString = request.getParameter("date_of_birth");
		Calendar dateOfBirth = null;
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
			dateOfBirth = Calendar.getInstance();
			dateOfBirth.setTime(date);
		} catch(ParseException e){
			out.println("Error while converting date of birth");
			return;
		}
		
		Contact contact = new Contact();
		contact.setName(name);
		contact.setEmail(email);
		contact.setAddress(address);
		contact.setDateOfBirth(dateOfBirth);
		
		try {
			ContactDao dao = new ContactDao();
			dao.create(contact);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Add New Contact</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>New Contact Added Successfully!</h1>");
		out.println("<hr/>");
		out.println("<p><b>Name: </b>" +contact.getName()+ "</p>");
		out.println("<p><b>Email: </b>" +contact.getEmail()+ "</p>");
		out.println("<p><b>Address: </b>" +contact.getAddress()+ "</p>");
		out.println("<p><b>Date Of Birth: </b>" +contact.getDateOfBirth().getTime().toString()+ "</p>");
		out.println("</body>");
		out.println("</html>");
		
	}
	
}
