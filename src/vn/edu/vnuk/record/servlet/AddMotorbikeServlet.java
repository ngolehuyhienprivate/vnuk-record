package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.MotorbikeDao;
import vn.edu.vnuk.record.jdbc.model.Motorbike;;


@WebServlet("/addMotorbike")
public class AddMotorbikeServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String brand = request.getParameter("brand");
		String model = request.getParameter("model");
		String engineSize = request.getParameter("engine_size");
		String gearbox = request.getParameter("gearbox");
		
		Motorbike motorbike = new Motorbike();
		motorbike.setBrand(brand);
		motorbike.setModel(model);
		try {
			int engine = Integer.parseInt(engineSize);
			motorbike.setEngineSize(engine);
		} catch(Exception e){
			out.println("Error while converting date of birth");
			return;
		}
		motorbike.setGearbox(gearbox);
		
		try {
			MotorbikeDao dao = new MotorbikeDao();
			dao.create(motorbike);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Add New Motorbike</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>New Motorbike Added Successfully!</h1>");
		out.println("<hr/>");
		out.println("<p><b>Brand: </b>" +motorbike.getBrand()+ "</p>");
		out.println("<p><b>Model: </b>" +motorbike.getModel()+ "</p>");
		out.println("<p><b>Engine size: </b>" +motorbike.getEngineSize()+ "</p>");
		out.println("<p><b>Gearbox: </b>" +motorbike.getGearbox()+ "</p>");
		out.println("</body>");
		out.println("</html>");
		
	}

}
